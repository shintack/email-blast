<?php

namespace App\Jobs;

use App\BlastReport;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\App;
use Swift_Mailer;
use Swift_SmtpTransport;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;
    private $user;
    private $jobs;
    private $masterTemplate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $user, $jobs, $masterTemplate)
    {
        $this->data = $data;
        $this->user = $user;
        $this->jobs = $jobs;
        $this->masterTemplate = $masterTemplate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {

        $useTemplate = false;
        if (view()->exists($this->masterTemplate->template)) {
            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
            $useTemplate = true;
        }

        $data = $this->data;
        $user = $this->user;
        $masterTemplate = $this->masterTemplate;
        $jobs = $this->jobs;
        if($jobs->config){
            // change config email here
            config([
                'mail.driver' => 'smtp',
                'mail.encryption' => 'tls',
                'mail.host' => $jobs->config->mail_host,
                'mail.port' => $jobs->config->mail_port,
                'mail.username' => $jobs->config->mail_username,
                'mail.password' => $jobs->config->mail_password,
                'mail.from.name' => $jobs->config->mail_from_name,
                'mail.from.address' => $jobs->config->mail_from_address,
                'mail.reply_to.name' => $jobs->config->mail_from_name,
                'mail.reply_to.address' => $jobs->config->mail_from_address,
            ]);
            // $app = App::getInstance();
            // $app->register('Illuminate\Mail\MailServiceProvider');
            Log::debug('message', config('mail'));

            $transport = new Swift_SmtpTransport($jobs->config->mail_host, $jobs->config->mail_port, 'tls');
            $transport->setUsername($jobs->config->mail_username);
            $transport->setPassword($jobs->config->mail_password);
            $smtp = new Swift_Mailer($transport);
            $mailer->setSwiftMailer($smtp);

            try {
                $sendmail = $mailer->send([], [], function ($message) use ($data, $masterTemplate) {
                    $message->from($data['from_email'], $data['from_name']);
                    $message->sender($data['from_email'], $data['from_name']);
                    $message->replyTo($data['from_email'], $data['from_name']);
                    $message->to($data['email'], $data['name']);
                    $message->subject($data['subject']);
                    $message->setBody($masterTemplate->content, 'text/html');
                });
                BlastReport::create([
                    'job_id' => $jobs->id,
                    'contact_id' => $user->id,
                    'status' => 1,
                    'metadata' => '',
                ]);
                Log::info('send email ', $data);
            } catch (\Throwable $th) {
                //throw $th;
                BlastReport::create([
                    'job_id' => $jobs->id,
                    'contact_id' => $user->id,
                    'status' => 0,
                    'metadata' => $th->getMessage(),
                ]);
                Log::error('failed send email ', $data);
            }

        }
        /*
        try {
            if ($useTemplate) {
                $beautymail->send($masterTemplate->template, ['user' => $user], function ($message) use ($data) {
                    $message
                        ->from($data['from_email'])
                        ->to($data['email'], $data['name'])
                        ->replyTo($data['from_email'])
                        ->subject($data['subject']);
                });
            } else {
                $sendmail = Mail::send([], [], function ($message) use ($data, $masterTemplate) {
                    $message->from($data['from_email'], $data['from_name']);
                    $message->sender($data['from_email'], $data['from_name']);
                    $message->replyTo($data['from_email'], $data['from_name']);
                    $message->to($data['email'], $data['name']);
                    $message->subject($data['subject']);
                    $message->setBody($masterTemplate->content, 'text/html');
                });
                // dd(config('mail'),$sendmail);
            }
            BlastReport::create([
                'job_id' => $jobs->id,
                'contact_id' => $user->id,
                'status' => 1,
                'metadata' => '',
            ]);
            Log::info('send email ', $data);
        } catch (\Throwable $th) {
            // throw $th;
            BlastReport::create([
                'job_id' => $jobs->id,
                'contact_id' => $user->id,
                'status' => 0,
                'metadata' => $th->getMessage(),
            ]);
            Log::error('failed send email ', $data);
        }
        */
    }
}
