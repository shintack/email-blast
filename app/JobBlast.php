<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobBlast extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'blast_id',
        'contact_id',
        'group_id',
        'config_id',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            $query->user_id = auth()->user()->id;
        });
    }

    public function template()
    {
        return $this->hasOne(EmailBlast::class, 'id', 'blast_id');
    }

    public function contact()
    {
        return $this->hasOne(Contact::class, 'id', 'contact_id');
    }

    public function group()
    {
        return $this->hasOne(GroupContact::class, 'id', 'group_id');
    }

    public function config()
    {
        return $this->hasOne(ConfigEmail::class, 'id', 'config_id');
    }

    public function report()
    {
        return $this->hasMany(BlastReport::class, 'job_id', 'id');
    }

    public function report_success()
    {
        return $this->hasMany(BlastReport::class, 'job_id', 'id')
            ->where('status', 1);
    }

    public function report_failed()
    {
        return $this->hasMany(BlastReport::class, 'job_id', 'id')
            ->where('status', 0);
    }
}
