<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Te7aHoudini\LaravelTrix\Traits\HasTrixRichText;

class EmailBlast extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'from_name',
        'from_email',
        'phone',
        'subject',
        'content',
        'template',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            $query->user_id = auth()->user()->id;
        });
    }

    // public function group()
    // {
    //     return $this->belongsTo(GroupContact::class, 'group_id', 'id');
    // }
}
