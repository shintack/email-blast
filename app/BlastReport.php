<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlastReport extends Model
{
    protected $fillable = [
        'job_id',
        'contact_id',
        'status',
        'metadata',
    ];

    public function job()
    {
        // return $this->hasOne(App\);
    }

    public function contact()
    {
        return $this->hasOne(Contact::class, 'id', 'contact_id');
    }

}
