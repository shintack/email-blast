<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupContact extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            $query->user_id = auth()->user()->id;
        });
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class, 'group_id', 'id');
    }
}
