<?php

namespace App\Http\Controllers;

use App\JobBlast;
use App\BlastReport;
use App\EmailBlast;
use App\GroupContact;
use App\User;
use App\Jobs\SendEmailJob;
use App\Exports\UserExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Dacastro4\LaravelGmail\Services\Message\Mail as Gmail;


class JobBlastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = JobBlast::orderBy('id', 'desc')->paginate(10);
        return view('job-blasts.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->validate([
                'blast_id' => 'required',
                'group_id' => 'required',
                'contact_id' => '',
                'config_id' => '',
            ]);
            $job = JobBlast::create($data);
            if ($job) {
                // run send email
                $this->actionSendmail($job);
                if ($request->redirect) {
                    return redirect($request->redirect)->with('success', 'Item created successfully!');
                } else {
                    return back()->with('success', 'Item created successfully!');
                }
            }
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobBlast  $jobBlast
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $query = BlastReport::query();

        if($request->status == 1){
            $query->where('status', 1);
        }elseif ($request->status == 0) {
            $query->where('status', 0);
        }

        $query->where('job_id', $id);

        if($request->export == 1){
            $raw = $query->get();
            $users = collect([]);
            foreach ($raw as $d) {
                $users->push($d->contact);
            }
            return Excel::download(new UserExport($users), 'user.xlsx');
        }

        $data = $query->latest()->paginate(10);

        $data->appends(['status' => $request->status]);

        return view('job-blasts.show-list', [
            'data' => $data
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobBlast  $jobBlast
     * @return \Illuminate\Http\Response
     */
    public function edit(JobBlast $jobBlast)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobBlast  $jobBlast
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobBlast $jobBlast)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobBlast  $jobBlast
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobBlast $jobBlast)
    {
        //
    }

    /**
     *
     */
    private function actionSendmail($jobs)
    {
        $masterTemplate = EmailBlast::where('id', $jobs->blast_id)->first();
        $reciver = GroupContact::where('id', $jobs->group_id)->first();
        $datas = $reciver->contacts;
        $useTemplate = false;
        if (view()->exists($masterTemplate->template)) {
            $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
            $useTemplate = true;
        }
        if($jobs->config_id==6969){
            foreach ($datas as $user) {
                try {
                    $to = $user->email;
                    $from = $masterTemplate->from_email;
                    $subject = $masterTemplate->subject;
                    $message = $masterTemplate->content;

                    $mail = new Gmail;
                    $mail->to( $to, $name = null );
                    $mail->from( $from, $name = null );
                    // $mail->cc( $cc, $name = null );
                    // $mail->bcc( $bcc, $name = null );
                    $mail->subject( $subject );
                    $mail->message( $message );
                    // $mail->view( 'view.name');
                    // $mail->attach( ...$path );
                    // $mail->priority( $priority )
                    // $mail->reply();
                    $mail->send();
                    // dd($mail);
                    BlastReport::create([
                        'job_id' => $jobs->id,
                        'contact_id' => $user->id,
                        'status' => 1,
                        'metadata' => '',
                    ]);
                    Log::info('send email ', $data);
                } catch (\Throwable $th) {
                    // throw $th;
                    BlastReport::create([
                        'job_id' => $jobs->id,
                        'contact_id' => $user->id,
                        'status' => 0,
                        'metadata' => $th->getMessage(),
                    ]);
                    Log::error('failed send email ', $data);
                }
            }
        }else{
            /**
             * CONSTANTA
             */
            define('DELAY', 60);
            define('LIMIT', 90);

            $limitPerMinute = LIMIT;
            $startLooping = LIMIT;
            $startDelay = 60;
            $delay = 2;
            foreach ($datas as $user) {
                
                if($startLooping > $limitPerMinute){
                    $startDelay += 60;
                    $limitPerMinute += LIMIT;
                    $delay = now()->addSeconds($startDelay);
                }
                
                ++$startLooping;

                $data = [
                    'id' => $user->id,
                    'email' => $user->email,
                    'name' => $user->name,
                    'from_name' => $masterTemplate->from_name,
                    'from_email' => $masterTemplate->from_email,
                    'reply_to' => $masterTemplate->from_email,
                    'subject' => $masterTemplate->subject,
                ];
                $job = (new SendEmailJob($data, $user, $jobs, $masterTemplate))->delay($delay);
                $this->dispatch($job);
            }
        }
    }

    public function resend($id)
    {
        return 'under development';
        $jobs = JobBlast::findOrFail($id);

        $masterTemplate = EmailBlast::where('id', $jobs->blast_id)->first();
        $reciver = GroupContact::where('id', $jobs->group_id)->first();

        $datas = $reciver->contact;
        dd($datas);
        $filter = $datas->filter(function ($key, $value) {
            dd($key);
        });

        dd($datas->count());
        dd($jobs, $masterTemplate, $reciver);
    }
}
