<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dacastro4\LaravelGmail\Services\Message\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->middleware('auth');
        return view('home');
    }

    public function cuy()
    {
        $user = \App\Contact::findOrFail(2);
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        $beautymail->send('email.info-virtual-class', ['user' => $user], function ($message) use ($user) {
            $message
                ->from('info@babastudio.com')
                ->to($user->email, $user->name)
                ->subject('Welcome!');
        });
        dd($beautymail);
    }

    public function composeGmail()
    {
        $to = 'insan.mmuttaqien@gmail.com';
        $from = 'info-prakerja@babastudio.com';
        $subject = 'tes gmail api';
        $message = 'ok tes isi email';

        $mail = new Mail;
        $mail->to( $to, $name = null );
        $mail->from( $from, $name = null );
        // $mail->cc( $cc, $name = null );
        // $mail->bcc( $bcc, $name = null );
        $mail->subject( $subject );
        $mail->message( $message );
        // $mail->view( 'view.name');
        // $mail->attach( ...$path );
        // $mail->priority( $priority )
        // $mail->reply();
        $mail->send();

        dd($mail);
    }
}
