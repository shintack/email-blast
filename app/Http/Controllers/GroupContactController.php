<?php

namespace App\Http\Controllers;

use App\GroupContact;
use Illuminate\Http\Request;

class GroupContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = GroupContact::paginate(10);
        return view('group-contacts.index', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required'
        ]);

        try {
            if (GroupContact::create($data)) {
                return back()->with('success', 'Item created successfully!');
            }
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GroupContact  $groupContact
     * @return \Illuminate\Http\Response
     */
    public function show(GroupContact $groupContact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GroupContact  $groupContact
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupContact $groupContact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GroupContact  $groupContact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupContact $groupContact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GroupContact  $groupContact
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupContact $groupContact, $id)
    {
        try {
            if (GroupContact::findOrFail($id)->delete()) {
                return back()->with('success', 'Item deleted successfully!');
            }
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
