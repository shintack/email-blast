<?php

namespace App\Http\Controllers;

use App\BlastReport;
use Illuminate\Http\Request;

class BlastReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BlastReport  $blastReport
     * @return \Illuminate\Http\Response
     */
    public function show(BlastReport $blastReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlastReport  $blastReport
     * @return \Illuminate\Http\Response
     */
    public function edit(BlastReport $blastReport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlastReport  $blastReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlastReport $blastReport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlastReport  $blastReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlastReport $blastReport)
    {
        //
    }
}
