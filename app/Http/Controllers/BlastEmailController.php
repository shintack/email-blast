<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\BlastEmailInfo;
use Mailgun\Mailgun;
use App\User;
use Illuminate\Support\Facades\Log;

class BlastEmailController extends Controller
{
    public function index()
    {
        return view('email.index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexOld()
    {
        // $data = User::all();

        // gelombang pertama user dibawah tahun 2019 jml total 1008
        // tahap pertama limit 500 offset 0
        // $datas = User::select('id', 'name', 'email')->where('created_at', '<', '2019-01-01')->limit(500)->offset(0)->get();
        // tahap kedua limit 500 offset 501 (BERHASIL CUMA 59)
        // $datas = User::select('id', 'name', 'email')->where('created_at', '<', '2019-01-01')->limit(500)->offset(501)->get();
        // tahap kedua lanjutan
        // $datas = User::select('id', 'name', 'email')->where('created_at', '<', '2019-01-01')->limit(450)->offset(560)->get();
        // tahap ketika limit 500 offset 1001
        // $datas = User::select('id', 'name', 'email')->where('created_at', '<', '2019-01-01')->limit(500)->offset(1001)->get();

        // gelombang kedua user tahun 2019 jml total 3179
        // tahap pertama limit 500 offset 0
        // $datas = User::select('id', 'name', 'email')->where('created_at', '>', '2019-01-01')->limit(500)->offset(0)->get();
        // tahap kedua limit 500 offset 501
        // $datas = User::select('id', 'name', 'email')->where('created_at', '>', '2019-01-01')->limit(500)->offset(501)->get();
        // tahap ketiga limit 500 offset 1001
        // $datas = User::select('id', 'name', 'email')->where('created_at', '>', '2019-01-01')->limit(500)->offset(1001)->get();
        // tahap keempat limit 500 offset 1501 -->lanjutkan disini
        // $datas = User::select('id', 'name', 'email')->where('created_at', '>', '2019-01-01')->limit(500)->offset(1501)->get();
        // tahap kelima limit 500 offset 2001
        // $datas = User::select('id', 'name', 'email')->where('created_at', '>', '2019-01-01')->limit(500)->offset(2001)->get();
        // tahap keenam limit 500 offset 2501
        // $datas = User::select('id', 'name', 'email')->where('created_at', '>', '2019-01-01')->limit(500)->offset(2501)->get();
        // tahap ketujuh limit 500 offset 3001
        // $datas = User::select('id', 'name', 'email')->where('created_at', '>', '2019-01-01')->limit(500)->offset(3001)->get();

        $datas = collect([
            (object) ['id' => 1, 'email' => 'insan.mm@yahoo.com', 'name' => 'insan'],
        ]);
        // dd($data->count());
        foreach ($datas as $user) {
            try {
                $data = ['id' => $user->id, 'email' => $user->email, 'name' => $user->name];
                $sendmail = Mail::send('email.template', $data, function ($message) use ($data) {
                    $message->from('info@babastudio.com', 'Babastudio');
                    $message->sender('info@babastudio.com', 'Babastudio');
                    $message->replyTo('info@babastudio.com', 'Babastudio');
                    $message->to($data['email'], $data['name'])->subject($data['name'] . ', Apakah ingin di bantu disalurkan pekerjaan oleh Baba Studio?');
                });
                Log::info('send email ', $data);
            } catch (\Throwable $th) {
                //throw $th;
                Log::error('failed send email ', $data);
            }
        }

        echo "ok";
    }

    /*
    $user = User::first();
        Mail::to($user)
            ->send(new BlastEmailInfo($user));
        return new BlastEmailInfo($user);
        // $mg = Mailgun::create(env('MAILGUN_API', '77d75a37b4a447a44ad8cb13036ef072-6140bac2-21080c09'));
        // $mg->messages()->send('mg.babastudio.com', [
        //     'from'    => 'Excited User <info@babastudio.com>',
        //     'to'      => 'Baz <insan.mmuttaqien@gmail.com>',
        //     'subject' => 'Hello',
        //     'text'    => 'Testing some Mailgun awesomness!'
        // ]);
        // dd($mg);
    */


    public function loadCsv()
    {
        if (($handle = fopen(public_path() . '/data.csv', 'r')) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                $user = User::where('email', $data[1])->first();
                if ($user == null) {
                    $user = new User();
                }
                $user->name = $data[0];
                $user->email = $data[1];
                $user->password = 'ok';
                $user->save();
                //saving to db logic goes here

            }
            fclose($handle);
        }
        echo "ok";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
