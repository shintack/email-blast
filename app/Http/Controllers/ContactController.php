<?php

namespace App\Http\Controllers;

use App\Contact;
use App\GroupContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Contact::query();

        if ($request->group_id) {
            $query->where('group_id', $request->group_id);
        }
        if ($request->keyword) {
            $query->where('name', 'like', "%$request->keyword%")
                ->orWhere('email', 'like', "%$request->keyword%");
        }

        $data = $query->paginate(10);
        $data->appends([
            'group_id' => $request->group_id,
            'keyword' => $request->keyword,
        ]);

        $groups = GroupContact::where('user_id', auth()->user()->id)->get();
        return view('contacts.index', [
            'data' => $data,
            'groups' => $groups,
            'contact' => null
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create', [
            'contact' => null
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if (!$request->hasFile('csv')) {
                $data = $request->validate([
                    'name' => 'required',
                    'email' => 'required',
                    'group_id' => '',
                    'phone' => '',
                    'note' => '',
                ]);

                if (Contact::create($data)) {
                    return back()->with('success', 'Item created successfully!');
                }
            } else {
                $path = storage_path('data');
                if (!File::isDirectory($path)) {
                    File::makeDirectory($path, 0777, true, true);
                }
                $file = $request->file('csv');
                //Move Uploaded File
                $destinationPath = $path;
                $file->move($destinationPath, $file->getClientOriginalName());

                $fullPathFile = $destinationPath . '/' . $file->getClientOriginalName();

                if (($handle = fopen($fullPathFile, 'r')) !== FALSE) {
                    while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                        $user = Contact::where('email', $data[1])->first();
                        if ($user == null) {
                            $user = new Contact();
                        }
                        $user->group_id = $request->group_id;
                        $user->name = $data[0];
                        $user->email = $data[1];
                        $user->save();
                        //saving to db logic goes here
                    }
                    fclose($handle);
                    return back()->with('success', 'Item created successfully!');
                } else {
                    return back()->with('error', 'Gagal membaca file csv');
                }
            }
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact, $id)
    {
        return view('contacts.create', [
            'contact' => $contact::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact, $id)
    {
        try {
            if ($contact::findOrFail($id)->delete()) {
                return back()->with('success', 'Item deleted successfully!');
            }
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
