<?php

namespace App\Http\Controllers;

use App\ConfigEmail;
use Illuminate\Http\Request;

class ConfigEmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('config-emails.index', [
            'configEmails' => ConfigEmail::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ConfigEmail::create($request->all());
        return back()->with('success', 'Item created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ConfigEmail  $configEmail
     * @return \Illuminate\Http\Response
     */
    public function show(ConfigEmail $configEmail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ConfigEmail  $configEmail
     * @return \Illuminate\Http\Response
     */
    public function edit(ConfigEmail $configEmail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ConfigEmail  $configEmail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConfigEmail $configEmail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ConfigEmail  $configEmail
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConfigEmail $configEmail, $id)
    {
        dd($id);
    }
}
