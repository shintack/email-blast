<?php

namespace App\Http\Controllers;

use App\EmailBlast;
use Illuminate\Http\Request;

class EmailBlastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = EmailBlast::paginate(10);
        $groups = \App\GroupContact::all();
        $configs = \App\ConfigEmail::all();
        return view('email-blast.index', [
            'data' => $data,
            'groups' => $groups,
            'configs' => $configs,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('email-blast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->validate([
                'from_name' => 'required',
                'from_email' => 'required',
                'subject' => 'required',
                'content' => 'required',
                'template' => '',
                'post-trixFields' => '',
                'attachment-post-trixFields' => '',
            ]);

            if (EmailBlast::create($data)) {
                return back()->with('success', 'Item created successfully!');
            }
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmailBlast  $emailBlast
     * @return \Illuminate\Http\Response
     */
    public function show(EmailBlast $emailBlast, $id)
    {
        $data = $emailBlast::findOrFail($id);
        return view('email-blast.show', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmailBlast  $emailBlast
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailBlast $emailBlast)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmailBlast  $emailBlast
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailBlast $emailBlast)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmailBlast  $emailBlast
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailBlast $emailBlast, $id)
    {
        try {
            if ($emailBlast::findOrFail($id)->delete()) {
                return back()->with('success', 'Item deleted successfully!');
            }
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
