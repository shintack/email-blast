@extends('beautymail::templates.minty')

@section('content')

@include('beautymail::templates.minty.contentStart')
<tr>
    <td width="100%" height="10"></td>
</tr>
<tr>
    <td class="paragraph">
        Hi {{ $user->name }},
        <br><br>
        Semoga semua sehat dan tetap #diRumahAja.
        <br>
        Seperti kamu….kamipun bosan di Rumah. Dan Ingin sekali agar {{ $user->name }}
        tetap bisa melanjutkan belajar dengan LIVE Instruktur.
        <br>
        Untuk itu Babastudio Baru saja membuat <b> LIVE ONLINE CLAS (Seperti ZOOM)
        Akan dimulai Hari Rabu tanggal 1 april 2020. </b>
        <br>
        1. Akan ada Instruktur Yg LIVE membimbing dan mengajar selama 1.5 Jam/Materi
        <br>
        2. Tiap hari akan ada berbagai Kelas seperti
        <br>
        a. Digital Marketing & SEO
        <br>
        b. Pemrograman Web & Mobile App
        <br>
        c. Desain Grafis.
        <br>
        Sementara kami menyediakan kelas ini terlebih dahulu.
        <br>
        3. Jadwal yang FIX dan bisa anda ikuti sesuai kebutuhan karena kelasnya akan Berulang.
        <br><br>
        Layanan baru senilai RP 3 Juta ini, dapat kamu nikmati GRATIS sebagai support agar kamu bisa lebih cepat mencapai tujuan belajar kamu.

        <br><br>
        Stay Safe & keep learning.
        <br>
        Babastudio
    </td>
</tr>
<tr>
    <td>
        @include('beautymail::templates.minty.button', ['text' => 'Yuk mulai belajar..', 'link' => 'https://academy.babastudio.com'])
    </td>
</tr>
<tr>
    <td>
        <h3>Jadwal LIVE ONLINE CLASS</h3>
        <img src="https://i.postimg.cc/90qwbtkv/jadwal-virtual-class.jpg" alt="" width="100%">
    </td>
</tr>
<tr>
    <td width="100%" height="25"></td>
</tr>
@include('beautymail::templates.minty.contentEnd')

@stop
