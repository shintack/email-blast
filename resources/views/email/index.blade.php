@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">

            buat group penerima email
            input penerima (satuan atau dari file csv)

            <div class="card">
                <div class="card-header">Email Blast</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="">input File CSV penerima email</label>
                        <input type="file" name="file" class="form-control">
                    </div>
                    <div class="form-group ">
                        <label for="">From Name</label>
                        <input type="text" name="text" class="form-control">
                    </div>
                    <div class="form-group ">
                        <label for="">From Email</label>
                        <input type="text" name="email" class="form-control">
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Content Email</div>
                <div class="card-body">
                    <textarea name="content" id="content" class="form-control textEditor"></textarea>
                </div>
                <button class="btn btn-primary" type="submit">Kirim</button>

            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')

@endsection

@section('scripts')
{{-- <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script> --}}
@endsection
