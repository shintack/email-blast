@extends('beautymail::templates.minty')

@section('content')

	@include('beautymail::templates.minty.contentStart')
		<tr>
			<td class="title">
				Mengenal Lebih Dekat Apa Itu Variabel Dan Tipe Data Pada PHP
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<tr>
			<td class="paragraph">
				Variabel dalam suatu program digunakan untuk menyimpan suatu nilai (value) atau data yang dapat digunakan nanti dalam suatu program. PHP memiliki cara sendiri untuk mendeklarasikan dan menyimpan variabel.
			</td>
		</tr>
		<tr>
			<td>
				@include('beautymail::templates.minty.button', ['text' => 'Selengkapnya..', 'link' => 'https://www.babastudio.com/blog/Variabel-dan-Tipe-Data-Pada-PHP'])
			</td>
		</tr>
		<tr>
			<td width="100%" height="25"></td>
		</tr>
	@include('beautymail::templates.minty.contentEnd')

@stop
