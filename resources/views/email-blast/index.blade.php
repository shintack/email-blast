@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-primary btn-sm pull-right" href="{{ route('create-template') }}"> Add </a>
                    Template Email
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                {{-- <th>Created At</th> --}}
                                <th>From</th>
                                <th>Subject</th>
                                {{-- <th>Jml Terkirim</th> --}}
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @dump($data) --}}
                            @foreach ($data as $row)
                                <tr>
                                    <td>{{ $row->id }}</td>
                                    {{-- <td>{{ $row->created_at }}</td> --}}
                                    <td>{{ $row->from_email }}</td>
                                    <td>{{ $row->subject }}</td>
                                    {{-- <td>100</td> --}}
                                    <td class="text-center">
                                        <a class="btn btn-primary btn-sm" href="{{ route('show-template', [ $row->id ]) }}" >View Content</a>
                                        <a class="btn btn-danger btn-sm" href="{{ route('delete-template', [ $row->id ]) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                        <button class="btn btn-success btn-sm" onclick="addJob('{{ $row->id }}')">Add Job</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add-job" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Job Send Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <form action="{{ route('save-job') }}" method="post">
        @csrf
      <div class="modal-body">
        <input type="hidden" name="blast_id" class="form-control" placeholder="blast id">
        <input type="hidden" name="redirect" class="form-control" value="{{ route('list-job') }}">
        <div class="form-group">
            <label for="">Pilih Group Contact</label>
            <select name="group_id" id="" class="form-control">
                @foreach ($groups as $group)
                    <option value="{{ $group->id }}">{{ $group->title }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="">Pilih Config Email</label>
            <select name="config_id" id="" class="form-control">
                <option value="">Default</option>
                <option value="6969"> <b> Kirim Via Gmail API </b> </option>
                @foreach ($configs as $config)
                    <option value="{{ $config->id }}">{{ $config->name }}</option>
                @endforeach
            </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
    <script>

        function addJob(id){
            $('[name="blast_id"]').val(id);
            $('#add-job').modal('show');
        }

    </script>
@endsection
