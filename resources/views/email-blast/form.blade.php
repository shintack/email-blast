<form action="{{ route('save-template') }}" method="post">
    @csrf
    <div class="form-group">
        <label for="">Subject </label>
        <input type="text" class="form-control" name="subject" required>
    </div>
    <div class="row">
        <div class="form-group col-md-6">
            <label for="">From Name </label>
            <input type="text" class="form-control" name="from_name" required>
        </div>
        <div class="form-group col-md-6">
            <label for="">From Email </label>
            <input type="email" class="form-control" name="from_email" required>
        </div>
    </div>
    <div class="form-group">
        <label for="">Content </label>
        <textarea name="content" class="form-control ckeditor" id="content" required></textarea>
        {{-- @trix(\App\EmailBlast::class, 'content') --}}
    </div>
    <div class="form-group">
        <label for="">Template View </label>
        <input type="text" class="form-control" name="template">
    </div>
    <button class="btn btn-primary float-right" type="submit"> Save </button>
</form>
