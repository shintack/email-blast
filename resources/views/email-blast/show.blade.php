@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <a class="btn btn-primary btn-sm" href="{{ route('list-template') }}"> Back </a>

            <div class="card">

                <div class="card-header">
                    <span>
                        From: <b> {{ $data->from_email }} </b>
                    </span>
                    <span class="float-right">
                        Subject: <b> {{ $data->subject }} </b>
                    </span>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    {!! $data->content !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
