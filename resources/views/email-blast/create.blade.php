@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-primary btn-sm pull-warning" href="{{ route('list-template') }}"> Back </a>
                    Create Template Email
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    @include('email-blast.form')

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
