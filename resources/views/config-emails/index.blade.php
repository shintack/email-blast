@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Config Email
                    <button class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#add-group">Add</button>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-hover table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th> name </th>
                                <th> mail_host </th>
                                <th> mail_port </th>
                                <th> mail_username </th>
                                <th> mail_password </th>
                                <th> mail_from_address </th>
                                <th> mail_from_name </th>
                                {{-- <th>Action</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($configEmails as $item)
                                <tr>
                                    <td> {{ $item->name }} </td>
                                    <td> {{ $item->mail_host }} </td>
                                    <td> {{ $item->mail_port }} </td>
                                    <td> {{ $item->mail_username }} </td>
                                    <td> {{ $item->mail_password }} </td>
                                    <td> {{ $item->mail_from_address }} </td>
                                    <td> {{ $item->mail_from_name }} </td>
                                    {{-- <td>
                                        <a class="btn btn-danger btn-sm" href="{{ route('config-email.destroy', [ $item->id ]) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                    </td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add-group" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Config</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('config-email.store') }}" method="post">
            @csrf
            <div class="modal-body">

                <div class="form-group">
                    <label for="">name</label>
                    <input required class="form-control" name="name" value="{{ old('name') }}">
                </div>
                <div class="form-group">
                    <label for="">mail_host</label>
                    <input required class="form-control" name="mail_host" value="{{ old('email_host') }}">
                </div>
                <div class="form-group">
                    <label for="">email_port</label>
                    <input required class="form-control" name="mail_port" value="{{ old('email_port') }}">
                </div>
                <div class="form-group">
                    <label for="">mail_username</label>
                    <input required class="form-control" name="mail_username" value="{{ old('email_username') }}">
                </div>
                <div class="form-group">
                    <label for="">mail_password</label>
                    <input required class="form-control" name="mail_password" value="{{ old('email_password') }}">
                </div>
                <div class="form-group">
                    <label for="">mail_from_address</label>
                    <input required class="form-control" name="mail_from_address" value="{{ old('email_from_address') }}">
                </div>
                <div class="form-group">
                    <label for="">mail_from_name</label>
                    <input required class="form-control" name="mail_from_name" value="{{ old('email_from_name') }}">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>

      </div>
    </div>
  </div>
@endsection
