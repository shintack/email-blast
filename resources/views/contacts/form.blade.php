<form action="{{ route('save-contact') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="">Name </label>
        <input type="text" class="form-control" value="{{ $contact ? $contact->name : old('name') }}" name="name">
    </div>
    <div class="form-group">
        <label for="">Email </label>
        <input type="email" class="form-control" value="{{ $contact ? $contact->email : old('email') }}" name="email">
    </div>
    <div class="form-group">
        <label for="">Phone </label>
        <input type="number" class="form-control" {{ $contact ? $contact->phone : old('phone') }} name="phone">
    </div>
    <div class="form-group">
        @if (request()->group_id)
            <input type="hidden" class="form-control" name="group_id" value="{{ request()->group_id }}">
        @else
            @php
                $groups = \App\GroupContact::where('user_id', auth()->user()->id)->get();
            @endphp
            <label for="">Pilih Group Contact</label>
            <select name="group_id" id="" class="form-control">
                @foreach ($groups as $item)
                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                @endforeach
            </select>
        @endif
    </div>
    <div class="form-group">
        <label for="">File CSV </label>
        <input type="file" class="form-control" name="csv" >
    </div>
    <button class="btn btn-primary float-right" type="submit"> Save </button>
</form>
