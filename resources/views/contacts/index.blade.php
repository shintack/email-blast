@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#add-group">Add</button>
                    Contact
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form class="form-inline">
                        <div class="form-group mb-2">
                            <select name="group_id" id="" class="form-control">
                                <option value="">Filter Group</option>
                                @foreach ($groups as $group)
                                    <option {{ request()->group_id == $group->id ? 'selected' : ''}} value="{{ $group->id }}">{{ $group->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="search" class="sr-only">Pencarian</label>
                            <input type="text" class="form-control" id="search" placeholder="Pencarian" name="keyword" value="{{ request()->keyword }}">
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Filter</button>
                    </form>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Group Contact</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @dump($data) --}}
                            @foreach ($data as $row)
                                <tr>
                                    <td>{{ $row->id }}</td>
                                    <td>{{ $row->group ? $row->group->title : '-'}}</td>
                                    <td>{{ $row->name }}</td>
                                    <td>{{ $row->email }}</td>
                                    <td>{{ $row->phone }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-success btn-sm" href="{{ route('edit-contact', [ $row->id ]) }}">Edit</a>
                                        <a class="btn btn-danger btn-sm" href="{{ route('delete-contact', [ $row->id ]) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                         {{-- <a class="btn btn-success btn-sm" href="{{ route('create-contact', [ 'group_id' => $row->id ]) }}" onclick="return confirm('Are you sure you want to delete this item?');">Add Contact</a> --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add-group" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Contact</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            @include('contacts.form')
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
@endsection
