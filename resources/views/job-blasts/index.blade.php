@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-primary btn-sm pull-right" href="{{ route('create-template') }}"> Add </a>
                    Job Send Email
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Created At</th>
                                <th>Template Email</th>
                                <th>Group Contact</th>
                                <th>Config Email</th>
                                <th>Terkirim</th>
                                <th>Gagal</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @dump($data) --}}
                            @foreach ($data as $row)
                                <tr>
                                    <td>{{ $row->id }}</td>
                                    <td>{{ $row->created_at }}</td>
                                    <td>{{ $row->template ? $row->template->subject . ' | ' . $row->template->from_email : '-' }}</td>
                                    <td>{{ $row->group ? $row->group->title : '' }}</td>
                                    <td>{{ $row->config ? $row->config->name : 'default' }}</td>
                                    <td>
                                        <a href="{{ route('show-job', [ 'id' => $row->id, 'status' => 1 ] ) }}" target="_blank" class="btn btn-success btn-sm" >
                                            {{ $row->report_success->count() }}
                                        </a>
                                    </td>
                                    <td> 
                                    <a href="{{ route('show-job', [ 'id' => $row->id, 'status' => 0 ] ) }}" target="_blank" class="btn btn-warning btn-sm" >
                                        {{ $row->report_failed->count() }}
                                    <a>
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-danger btn-sm" href="{{ route('resend-job', [ $row->id ]) }}" onclick="return confirm('Are you sure you want to action this item?');">Kirim Ulang</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add-job" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Job Send Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @php
        $groups = \App\GroupContact::where('user_id', auth()->user()->id)->get();
    @endphp
    <form action="{{ route('save-job') }}" method="post">
        @csrf
      <div class="modal-body">
        <div class="form-group">
            <label for="">Pilih Group Contact</label>
            <select name="group_id" id="" class="form-control">
                @foreach ($groups as $group)
                    <option value="{{ $group->id }}">{{ $group->title }}</option>
                @endforeach
            </select>
            <input type="hidden" name="blast_id" class="form-control" placeholder="blast id">

            <input type="hidden" name="redirect" class="form-control" value="{{ route('list-job') }}">

        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')

@endsection
