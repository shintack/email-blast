@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <!-- <a class="btn btn-primary btn-sm pull-right" href="{{ route('create-template') }}"> Add </a> -->
                    <div class="row">
                        <div class="col-6">
                            Detail report Send Email <b># {{ $data->total() }} </b>
                        </div>
                        <div class="col-6">
                        <form action="" method="GET" class="form-inline float-right">
                            <div class="form-check mr-3">
                                <input class="form-check-input" {{ request('status') == 1 ? 'checked' : '' }} type="radio" name="status" id="exampleRadios1" value="1" >
                                <label class="form-check-label" for="exampleRadios1">
                                    Terkirim
                                </label>
                            </div>
                            <div class="form-check mr-3">
                                <input class="form-check-input" {{ request('status') == 0 ? 'checked' : '' }} type="radio" name="status" id="exampleRadios2" value="0" >
                                <label class="form-check-label" for="exampleRadios2">
                                    Gagal Terkirim
                                </label>
                            </div>
                            <div class="form-check mr-3">
                                <input class="form-check-input" name="export" type="checkbox" value="1" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Export
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">Action</button>
                        </form>
                        </div>
                    </div>
                    
                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Created At</th>
                                <th>Contact</th>
                                <th>LOG</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @dump($data) --}}
                            @foreach ($data as $row)
                                <tr>
                                    <td>{{ $row->id }}</td>
                                    <td>{{ $row->created_at }}</td>
                                    <td>{{ $row->contact ? $row->contact->email : 'Kontak dihapus' }}</td>
                                    <td>{{ $row->metadata }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add-job" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Job Send Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @php
        $groups = \App\GroupContact::where('user_id', auth()->user()->id)->get();
    @endphp
    <form action="{{ route('save-job') }}" method="post">
        @csrf
      <div class="modal-body">
        <div class="form-group">
            <label for="">Pilih Group Contact</label>
            <select name="group_id" id="" class="form-control">
                @foreach ($groups as $group)
                    <option value="{{ $group->id }}">{{ $group->title }}</option>
                @endforeach
            </select>
            <input type="hidden" name="blast_id" class="form-control" placeholder="blast id">

            <input type="hidden" name="redirect" class="form-control" value="{{ route('list-job') }}">

        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')

@endsection
