<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cuy', 'HomeController@cuy')->name('cuy');

Route::group(['middleware' => ['auth']], function () {

    Route::group(['prefix' => 'contacts'], function () {
        Route::group(['prefix' => 'groups'], function () {
            Route::get('/', 'GroupContactController@index')->name('list-group-contact');
            Route::post('/', 'GroupContactController@store')->name('save-group-contact');
            Route::put('/', 'GroupContactController@update')->name('update-group-contact');
            Route::get('/{id}/delete/', 'GroupContactController@destroy')->name('delete-group-contact');
        });

        Route::get('/', 'ContactController@index')->name('list-contact');
        Route::get('/create', 'ContactController@create')->name('create-contact');
        Route::post('/', 'ContactController@store')->name('save-contact');
        Route::get('/{id}', 'ContactController@edit')->name('edit-contact');
        Route::put('/', 'ContactController@update')->name('update-contact');
        Route::get('/{id}/delete/', 'ContactController@destroy')->name('delete-contact');
    });

    Route::group(['prefix' => 'template-email'], function () {
        Route::get('/', 'EmailBlastController@index')->name('list-template');
        Route::get('/create', 'EmailBlastController@create')->name('create-template');
        Route::get('/{id}', 'EmailBlastController@show')->name('show-template');
        Route::post('/', 'EmailBlastController@store')->name('save-template');
        Route::put('/', 'EmailBlastController@update')->name('update-template');
        Route::get('/{id}/delete/', 'EmailBlastController@destroy')->name('delete-template');
    });

    Route::group(['prefix' => 'job-send-email'], function () {
        Route::get('/', 'JobBlastController@index')->name('list-job');
        Route::get('/create', 'JobBlastController@create')->name('create-job');
        Route::get('/{id}', 'JobBlastController@show')->name('show-job');
        Route::post('/', 'JobBlastController@store')->name('save-job');
        Route::put('/', 'JobBlastController@update')->name('update-jon');
        Route::get('/{id}/delete/', 'JobBlastController@destroy')->name('delete-job');

        Route::get('/{id}/resend', 'JobBlastController@resend')->name('resend-job');

    });

    Route::resource('config-email', 'ConfigEmailController');

    Route::get('/oauth/gmail/callback', function (){
        LaravelGmail::makeToken();
        return redirect()->to('/');
    });

    Route::group(['prefix' => 'gmail'], function () {

        Route::get('/login', function (){
            return LaravelGmail::redirect();
        })->name('gmail.login');

        Route::get('/logout', function (){
            LaravelGmail::logout(); //It returns exception if fails
            return redirect()->to('/home');
        })->name('gmail.logout');
                
        Route::get('/inbox', function () {
            // dd(request());
            $messages = LaravelGmail::message()->unread()->preload()->all();
            echo '<ul>';
            foreach ( $messages as $message ) {
                $body = $message->getHtmlBody();
                echo '<li>' . $subject = $message->getSubject() . '</li>';
            }
            echo '</ul>';
        })->name('gmail.inbox');

        Route::get('/compose', 'HomeController@composeGmail');
    
    });


});

// Route::get('/tes-mail', 'BlastEmailController@index');

// Route::get('/email', 'BlastEmailController@index');


// Route::get('/load-csv', 'BlastEmailController@loadCsv');
