<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_emails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string(strtolower('MAIL_HOST'));
            $table->string(strtolower('MAIL_PORT'));
            $table->string(strtolower('MAIL_USERNAME'));
            $table->string(strtolower('MAIL_PASSWORD'));
            $table->string(strtolower('MAIL_FROM_ADDRESS'));
            $table->string(strtolower('MAIL_FROM_NAME'));
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_emails');
    }
}
